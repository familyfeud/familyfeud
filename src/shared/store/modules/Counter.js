const Counter = {
  namespaced: true,
  state: {
    main: 3,
    team1: {
      name: 'Default',
      points: ''
    },
    team2: {
      name: 'Default',
      points: ''
    }
  },
  mutations: {
    DECREMENT_MAIN_COUNTER (state) {
      state.main--
    },
    INCREMENT_MAIN_COUNTER (state) {
      state.main++
    },
    SET_TEAM1_NAME (state, teamName) {
      state.team1.name = teamName.payload
    },
    SET_TEAM2_NAME (state, teamName) {
      state.team2.name = teamName.payload
    }
  },
  actions: {
    someAsyncTask ({ commit }) {
      commit('INCREMENT_MAIN_COUNTER')
    },
    setTeamOneName ({ commit }, payload) {
      let teamName = payload.payload[0]
      commit('SET_TEAM1_NAME', teamName)
    },
    setTeamTwoName ({ commit }, payload) {
      let teamName = payload.payload[0]
      commit('SET_TEAM2_NAME', teamName)
    }
  }
}
export default Counter

