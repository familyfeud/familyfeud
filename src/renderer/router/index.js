import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'admin-view',
      component: require('@/components/views/AdminView')
    },
    {
      path: '/game',
      name: 'game-view',
      component: require('@/components/views/GameView')
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
